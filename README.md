# Project description

## Technical notes

This project was developed with:
* Node v18.12.1.
* npm v8.19.2.

## Installation

1. Clone the [repo](git@gitlab.com:ricardoleoncorreia/health-tips-challenge.git).
1. Install dependencies with `npm install`.
1. Run the dev server `npm start`.
1. Open a web browser with url `http://localhost:4200/`.

**NOTE:** Backend port should be 8080. Otherwise, the `proxy.conf.json` file needs to be updated with the corresponding port.

## Running unit tests

1. Run `npm test`.

## Design Assumptions

* App features:
  * The root path `/` redirects to the overview page `/health-tips`.
  * On details page reload, the user will be redirected to the overview page (due to the randomness of list). 

* Time Handling:
  * Weeks start on Monday.
  * The `TimeUtils` class serves as an adapter for [Luxon](https://moment.github.io/luxon/#/). In case the library needs to change (deprecated, new purchase, etc), the changes will be limited to this class. The core app won't require modifications.
  * Across the app, only ISO datetime strings are used to avoid mix of date types or formats.

* CSS:
  * Normalize CSS was installed to unify styles across browsers.
  * [BEM](https://getbem.com/) was used for CSS naming convention.

* Javascript:
  * Only a few unit tests were implemented to cover different use cases (`HealthTipVotingComponent`, `HealthTipsService` and `HealthTipsEffects`).
  * `HealthTips` class goal is to adapt the DTO to a useful model for the core app. If the DTO changes (e.g. property renaming), this class should handle these without affecting the core app.
  * Lazy Loading was implemented for the Health Tips Module.

* Git Strategy:
  * No branch statrategy was used. Only the `main` branch was used.
  * [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) was used as a commit message standard.

## Improvements for the project

* Install code formaters and linters like [Prettier](https://prettier.io/) and [ESLint](https://eslint.org/).
* Add translations for other languages using libraries like [ngx-translate](https://github.com/ngx-translate/core).
* Use tools like [Webpack Bundle Analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer) to monitor the size of each particular module or library.
* Add Well favicon to show it in the browser's tab.
* Increase test coverage.
