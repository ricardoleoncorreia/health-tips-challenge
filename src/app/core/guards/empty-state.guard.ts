import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map, tap } from 'rxjs';

import { AppStore } from '@core/store/app-store';
import { selectHealthTipsList } from '@features/health-tips/store/health-tips.selector';

export const emptyStoreGuard: CanActivateFn = () => {
  const router = inject(Router);
  const store = inject(Store<AppStore>);
  return store.pipe(
    select(selectHealthTipsList),
    map(list => list.length > 0),
    tap(nonEmptyStore => {
      if (nonEmptyStore) {
        return;
      }
      router.navigate(['/health-tips']);
    })
  )
};
