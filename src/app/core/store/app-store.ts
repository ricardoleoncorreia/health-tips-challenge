import { HealthTipsStore } from '@features/health-tips/interfaces/health-tips-store';

export interface AppStore {
  healthTips: HealthTipsStore;
}
