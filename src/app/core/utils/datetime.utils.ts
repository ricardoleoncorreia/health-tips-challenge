import { DateTime } from 'luxon';

export class DateTimeUtils {
  static isToday(isoDatetime: string): boolean {
    const datetime = DateTime.fromISO(isoDatetime);
    const localTodayStart = DateTimeUtils.getStartOf('day');
    return datetime >= localTodayStart;
  }

  static isThisWeek(isoDatetime: string) {
    const datetime = DateTime.fromISO(isoDatetime);
    const localTodayStart = DateTimeUtils.getStartOf('day');
    const localWeekStart = DateTimeUtils.getStartOf('week');
    return localWeekStart <= datetime && datetime < localTodayStart;
  }

  static isEarlierThanThisWeek(isoDatetime: string) {
    const datetime = DateTime.fromISO(isoDatetime);
    const localWeekStart = DateTimeUtils.getStartOf('week');
    return datetime < localWeekStart;
  }

  private static getStartOf(unit: 'day' | 'week'): DateTime {
    return DateTime.local().startOf(unit);
  }
}
