import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { HealthTip } from '@features/health-tips/models/health-tip';

@Component({
  selector: 'app-health-tip-overview-group',
  templateUrl: 'health-tip-overview-group.component.html',
  styleUrls: ['./health-tip-overview-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HealthTipOverviewGroupComponent {
  @Input() groupTitle = '';
  @Input() tips: HealthTip[] = [];
}
