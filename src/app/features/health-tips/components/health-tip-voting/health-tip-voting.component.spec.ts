import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';

import { HealthTipVotingComponent } from './health-tip-voting.component';

describe('HealthTipVotingComponent', () => {
  let fixture: ComponentFixture<HealthTipVotingComponent>;
  let component: HealthTipVotingComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HealthTipVotingComponent],
      imports: [MatIconModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthTipVotingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have an up-vote icon`, () => {
    const compiled = fixture.nativeElement as HTMLElement;

    expect(compiled.querySelector('.container__up-vote')).toBeTruthy();
  });

  describe('when user clicks on the up-vote icon', () => {
    it('should emit an up-vote', () => {
      spyOn(component.upVote, 'emit');
      const compiled = fixture.nativeElement as HTMLElement;
      const upVoteButton = compiled.querySelector('.container__up-vote') as HTMLButtonElement;

      upVoteButton.click();

      expect(component.upVote.emit).toHaveBeenCalled();
    });
  });
});
