import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-health-tip-voting',
  templateUrl: 'health-tip-voting.component.html',
  styleUrls: ['./health-tip-voting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HealthTipVotingComponent {
  @Input() upVotes = 0;
  @Input() downVotes = 0;
  @Input() disabled = false;

  @Output() readonly upVote = new EventEmitter<void>();
  @Output() readonly downVote = new EventEmitter<void>();

  onUpVote(): void {
    this.upVote.emit();
  }

  onDownVote(): void {
    this.downVote.emit();
  }
}
