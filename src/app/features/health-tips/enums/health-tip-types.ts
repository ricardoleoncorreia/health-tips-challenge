export enum HealthTipType {
  DoctorHealthTip = 'DoctorHealthTip',
  PsychoHealthTip = 'PsychoHealthTip',
  FamilyHealthTip = 'FamilyHealthTip',
  FitnessHealthTip = 'FitnessHealthTip',
  InsuranceHealthTip = 'InsuranceHealthTip'
}
