import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthTipsDetailsComponent } from './pages/health-tips-details/health-tips-details.component';
import { HealthTipsOverviewComponent } from './pages/health-tips-overview/health-tips-overview.component';
import { emptyStoreGuard } from '@core/guards/empty-state.guard';

const routes: Routes = [
  {
    path: '',
    component: HealthTipsOverviewComponent
  },
  {
    path: ':id',
    component: HealthTipsDetailsComponent,
    canActivate: [emptyStoreGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthTipsRoutingModule { }
