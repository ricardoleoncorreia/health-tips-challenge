import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { HealthTipsRoutingModule } from './health-tips-routing.module';
import { HealthTipsDetailsComponent } from './pages/health-tips-details/health-tips-details.component';
import { HealthTipsOverviewComponent } from './pages/health-tips-overview/health-tips-overview.component';
import { HealthTipsService } from './services/health-tips.service';
import { HealthTipOverviewGroupComponent } from './components/health-tip-overview-group/health-tip-overview-group.component';
import { HealthTipVotingComponent } from './components/health-tip-voting/health-tip-voting.component';
import { healthTipsReducer } from './store/health-tips.reducer';
import { HealthTipsEffects } from './store/health-tips.effects';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    HealthTipsOverviewComponent,
    HealthTipsDetailsComponent,
    HealthTipOverviewGroupComponent,
    HealthTipVotingComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    StoreModule.forFeature('healthTips', healthTipsReducer),
    EffectsModule.forFeature([HealthTipsEffects]),
    HealthTipsRoutingModule,
    SharedModule
  ],
  providers: [HealthTipsService]
})
export class HealthTipsModule { }
