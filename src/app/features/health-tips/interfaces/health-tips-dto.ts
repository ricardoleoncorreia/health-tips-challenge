import { HealthTipType } from '../enums/health-tip-types';

export interface HealthTipDto {
  datetime: string;
  downVotes: number;
  id: number;
  level: string;
  text: string;
  title: string;
  type: HealthTipType;
  upVotes: number;
}
