import { HealthTip } from '../models/health-tip';

export interface HealthTipsList {
  today: HealthTip[];
  thisWeek: HealthTip[];
  earlier: HealthTip[];
}
