import { HealthTip } from '@features/health-tips/models/health-tip';

export interface HealthTipsStore {
  failed: boolean;
  loading: boolean;
  list: HealthTip[];
}
