import { HealthTipType } from '../enums/health-tip-types';
import { HealthTipDto } from '../interfaces/health-tips-dto';

export class HealthTip {
  readonly datetime: string;
  readonly downVotes: number;
  readonly id: number;
  readonly level: string;
  readonly text: string;
  readonly title: string;
  readonly type: HealthTipType;
  readonly upVotes: number;

  constructor(healthTipDto: HealthTipDto) {
    this.datetime = healthTipDto.datetime;
    this.downVotes = healthTipDto.downVotes;
    this.id = healthTipDto.id;
    this.level = healthTipDto.level;
    this.text = healthTipDto.text;
    this.title = healthTipDto.title;
    this.type = healthTipDto.type;
    this.upVotes = healthTipDto.upVotes;
  }
}
