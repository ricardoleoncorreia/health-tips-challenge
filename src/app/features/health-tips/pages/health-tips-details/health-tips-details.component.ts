import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subject, takeUntil } from 'rxjs';

import { AppStore } from '@core/store/app-store';
import { HealthTipsVote } from '@features/health-tips/interfaces/health-tips-vote';
import { HealthTip } from '@features/health-tips/models/health-tip';
import { healthTipVote } from '@features/health-tips/store/health-tips.actions';
import { selectHealthTipById } from '@features/health-tips/store/health-tips.selector';

@Component({
  selector: 'app-health-tips-details',
  templateUrl: 'health-tips-details.component.html',
  styleUrls: ['./health-tips-details.component.scss']
})
export class HealthTipsDetailsComponent implements OnInit, OnDestroy {
  healthTip?: HealthTip;

  private readonly destroy = new Subject<void>();

  constructor(private readonly route: ActivatedRoute, private readonly store: Store<AppStore>) {}

  ngOnInit(): void {
    const tipId = Number(this.route.snapshot.paramMap.get('id'));
    const selectTipById = selectHealthTipById({ id: tipId });
    this.store.pipe(
      takeUntil(this.destroy),
      select(selectTipById)
    ).subscribe(tip => this.healthTip = tip);
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  onUpVote(): void {
    this.saveVote('UP');
  }

  onDownVote(): void {
    this.saveVote('DOWN');
  }

  private saveVote(voteType: HealthTipsVote): void {
    if (!this.healthTip) {
      throw new Error('No health tip was provided');
    }

    this.store.dispatch(healthTipVote({ id: this.healthTip.id, voteType }));
  }
}
