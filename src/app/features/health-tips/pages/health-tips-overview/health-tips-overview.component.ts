import { Component, OnInit } from '@angular/core';
import { map, Observable, tap } from 'rxjs';
import { select, Store } from '@ngrx/store';

import { DateTimeUtils } from '@core/utils/datetime.utils';
import { AppStore } from '@core/store/app-store';
import { HealthTip } from '@features/health-tips/models/health-tip';
import { HealthTipsList as HealthTipsOverviewList } from '@features/health-tips/interfaces/health-tips-list';
import { selectHealthTipsFailed, selectHealthTipsList, selectHealthTipsLoading } from '@features/health-tips/store/health-tips.selector';
import { healthTipListLoad } from '@features/health-tips/store/health-tips.actions';

@Component({
  selector: 'app-health-tips-overview',
  templateUrl: './health-tips-overview.component.html',
  styleUrls: ['./health-tips-overview.component.scss']
})
export class HealthTipsOverviewComponent implements OnInit {
  readonly healthTipsOverviewList$: Observable<HealthTipsOverviewList>;
  readonly failed$: Observable<boolean>;
  readonly loading$: Observable<boolean>;

  constructor(private readonly store: Store<AppStore>) {
    this.failed$ = this.store.pipe(select(selectHealthTipsFailed));
    this.loading$ = this.store.pipe(select(selectHealthTipsLoading));
    this.healthTipsOverviewList$ = this.store.pipe(
      select(selectHealthTipsList),
      map(tips => this.buildOverviewList(tips))
    );
  }

  ngOnInit(): void {
    this.store.dispatch(healthTipListLoad());
  }

  private buildOverviewList(tips: HealthTip[]): HealthTipsOverviewList {
    const emptyList: HealthTipsOverviewList = { today: [], thisWeek: [], earlier: [] };
    return tips.reduce((list, tip) => {
      if (DateTimeUtils.isToday(tip.datetime)) {
        list.today.push(tip);
      } else if (DateTimeUtils.isThisWeek(tip.datetime)) {
        list.thisWeek.push(tip);
      } else if (DateTimeUtils.isEarlierThanThisWeek(tip.datetime)) {
        list.earlier.push(tip);
      } else {
        throw new Error('Invalid Datetime');
      }
      return list;
    }, emptyList);
  }
}
