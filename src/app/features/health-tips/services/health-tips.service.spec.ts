import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { HealthTipsTestHelper } from '../unit-testing/health-tips-test-helper';
import { HealthTipsService } from './health-tips.service';

describe('HealthTipsService', () => {
  let service: HealthTipsService;
  let httpController: HttpTestingController;
  let healthTipsTestHelper: HealthTipsTestHelper;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [HealthTipsService],
      imports: [HttpClientTestingModule]
    }).compileComponents();

    healthTipsTestHelper = new HealthTipsTestHelper();
    service = TestBed.inject(HealthTipsService);
    httpController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpController.verify();
  });

  it('should create service', () => {
    expect(service).toBeTruthy();
  });

  describe('#getHealthTipsList', () => {
    const url = '/api/tips/random';

    it('should return a list of health tips on request success', () => {
      service.getHealthTipsList().subscribe({
        next: res => expect(res).toEqual(healthTipsTestHelper.tipsList),
        error: () => fail('http request should NOT fail')
      });

      const req = httpController.expectOne(url);
      expect(req.request.method).toBe('GET');

      req.flush(healthTipsTestHelper.tipsDtoList);
    });

    it('should return the same error on request failure', () => {
      const errorMsg = '500 error';

      service.getHealthTipsList().subscribe({
        next: () => fail('http request should fail'),
        error: (error) => {
          expect(error.status).toBe(500);
          expect(error.error).toBe(errorMsg);
        }
      });

      const req = httpController.expectOne(url);
      expect(req.request.method).toBe('GET');

      req.flush(errorMsg, { status: 500, statusText: 'Server Error' });
    });
  });
});
