import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';

import { HealthTipDto } from '../interfaces/health-tips-dto';
import { HealthTip } from '../models/health-tip';
import { HealthTipsVote } from '../interfaces/health-tips-vote';

@Injectable()
export class HealthTipsService {
  constructor(private readonly http: HttpClient) { }

  getHealthTipsList(): Observable<HealthTip[]> {
    return this.http.get<HealthTipDto[]>('/api/tips/random').pipe(
      map(res => res.map(tipDto => new HealthTip(tipDto)))
    );
  }

  getHealthTip(id: number): Observable<HealthTip> {
    return this.http.get<HealthTipDto>(`/api/tips/${id}`).pipe(
      map(tipDto => new HealthTip(tipDto))
    );
  }

  voteTip(id: number, voteType: HealthTipsVote): Observable<HealthTip> {
    return this.http.get<HealthTipDto>(`/api/tips/${id}/vote/${voteType}`).pipe(
      map(tipDto => new HealthTip(tipDto))
    );
  }
}
