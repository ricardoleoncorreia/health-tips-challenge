import { createAction, props } from '@ngrx/store';

import { HealthTip } from '../models/health-tip';
import { HealthTipsVote } from '../interfaces/health-tips-vote';

export const HEALTH_TIP_LIST_LOAD = '[Health Tips Overview Component] Health Tip List Load';
export const HEALTH_TIP_LIST_LOADED = '[Health Tips Overview Component] Health Tip List Loaded';
export const SHOW_ERROR_MESSAGE = '[Health Tips Overview Component] Show Error Message';
export const SHOW_SPINNER = '[Health Tips Overview Component] Show Spinner';
export const HIDE_SPINNER = '[Health Tips Overview Component] Hide Spinner';
export const HEALTH_TIP_VOTE = '[Health Tips Details Component] Health Tip Vote';
export const HEALTH_TIP_UPDATED = '[Health Tips Details Component] Health Tip Updated';

export const healthTipListLoad = createAction(HEALTH_TIP_LIST_LOAD);
export const healthTipListLoaded = createAction(HEALTH_TIP_LIST_LOADED, props<{ tips: HealthTip[] }>());
export const showErrorMessage = createAction(SHOW_ERROR_MESSAGE, props<{ message: string; }>());
export const showSpinner = createAction(SHOW_SPINNER);
export const hideSpinner = createAction(HIDE_SPINNER);
export const healthTipVote = createAction(HEALTH_TIP_VOTE, props<{ id: number; voteType: HealthTipsVote }>());
export const healthTipUpdated = createAction(HEALTH_TIP_UPDATED, props<{ tip: HealthTip }>());
