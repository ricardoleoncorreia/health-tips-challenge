import { Actions } from '@ngrx/effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AppStore } from '@core/store/app-store';
import { HealthTipsEffects } from './health-tips.effects';
import { HealthTipsService } from '../services/health-tips.service';
import { healthTipListLoad, healthTipListLoaded } from './health-tips.actions';
import { HealthTipsTestHelper } from '../unit-testing/health-tips-test-helper';

describe('HealthTipsEffects', () => {
  let store: MockStore;
  let actions$: Actions;
  let effects$: HealthTipsEffects;
  let healthTipsTestHelper: HealthTipsTestHelper;
  let healthTipsService: {
    getHealthTipsList: jasmine.Spy;
  };

  beforeEach(() => {
    healthTipsTestHelper = new HealthTipsTestHelper();

    healthTipsService = jasmine.createSpyObj<HealthTipsService>('HealthTipsService', ['getHealthTipsList']);
    healthTipsService.getHealthTipsList.and.returnValue(of(healthTipsTestHelper.tipsDtoList));

    const initialState: AppStore = {
      healthTips: {
        failed: false,
        loading: false,
        list: []
      }
    };

    TestBed.configureTestingModule({
      providers: [
        provideMockStore<AppStore>({ initialState }),
        provideMockActions(() => actions$),
        HealthTipsEffects,
        { provide: HealthTipsService, useValue: healthTipsService }
      ],
    });

    store = TestBed.inject(MockStore);
    actions$ = TestBed.inject(Actions);
    effects$ = TestBed.inject(HealthTipsEffects);
  });

  describe('when executing load tips effect', () => {
    it('should fetch health tips list', (done) => {
      actions$ = of(healthTipListLoad());
      effects$.loadTips$.subscribe(() => {
        expect(healthTipsService.getHealthTipsList).toHaveBeenCalled();
        done();
      });
    });

    it('should load fetched health tips list', (done) => {
      actions$ = of(healthTipListLoad());
      effects$.loadTips$.subscribe(action => {
        expect(action).toEqual(healthTipListLoaded({ tips: healthTipsTestHelper.tipsDtoList }));
        done();
      });
    });
  });

});
