import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, filter, map, of, switchMap } from 'rxjs';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { AppStore } from '@core/store/app-store';
import {
  showErrorMessage,
  healthTipListLoaded,
  HEALTH_TIP_LIST_LOAD,
  healthTipUpdated,
  HEALTH_TIP_VOTE,
  showSpinner,
  HEALTH_TIP_LIST_LOADED,
  HEALTH_TIP_UPDATED,
  hideSpinner,
  SHOW_ERROR_MESSAGE
} from './health-tips.actions';
import { HealthTipsService } from '../services/health-tips.service';
import { selectHealthTipsList } from './health-tips.selector';

@Injectable()
export class HealthTipsEffects {
  readonly loadTips$ = createEffect(() => this.actions$.pipe(
    ofType(HEALTH_TIP_LIST_LOAD),
    concatLatestFrom(() => this.store.select(selectHealthTipsList)),
    filter(([_, tips]) => tips.length === 0),
    switchMap(() => this.healthTipsService.getHealthTipsList()
      .pipe(
        map(tips => healthTipListLoaded({ tips })),
        catchError(({ message }: HttpErrorResponse) => of(showErrorMessage({ message })))
      )
    )
  ));

  readonly updateTip$ = createEffect(() => this.actions$.pipe(
    ofType(HEALTH_TIP_VOTE),
    switchMap(({ id, voteType }) => this.healthTipsService.voteTip(id, voteType)
      .pipe(map(tip => healthTipUpdated({ tip })))
    )
  ));

  readonly showSpinner$ = createEffect(() => this.actions$.pipe(
    ofType(HEALTH_TIP_LIST_LOAD, HEALTH_TIP_VOTE),
    concatLatestFrom(() => this.store.select(selectHealthTipsList)),
    filter(([_, tips]) => tips.length === 0),
    map(() => showSpinner()),
  ));

  readonly hideSpinner$ = createEffect(() => this.actions$.pipe(
    ofType(HEALTH_TIP_LIST_LOADED, HEALTH_TIP_UPDATED, SHOW_ERROR_MESSAGE),
    map(() => hideSpinner()),
  ));

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppStore>,
    private readonly healthTipsService: HealthTipsService
  ) { }
}
