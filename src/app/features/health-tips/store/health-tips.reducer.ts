import { createReducer, on } from '@ngrx/store';

import { showErrorMessage, healthTipListLoaded, healthTipUpdated, showSpinner, hideSpinner } from './health-tips.actions';
import { HealthTipsStore } from '../interfaces/health-tips-store';

export const initialState: HealthTipsStore = { failed: false, loading: false, list: [] };

export const healthTipsReducer = createReducer(
  initialState,
  on(showErrorMessage, (state) => ({ ...state, failed: true })),
  on(showSpinner, (state) => ({ ...state, loading: true })),
  on(hideSpinner, (state) => ({ ...state, loading: false })),
  on(healthTipListLoaded, (state, { tips }) => ({ ...state, list: [...tips] })),
  on(healthTipUpdated, (state, { tip }) => {
    const tipIndex = state.list.findIndex(item => item.id === tip.id);
    if (tipIndex === -1) {
      return { ...state };
    }

    const clonedList = [...state.list];
    clonedList[tipIndex] = tip;
    return { ...state, list: clonedList };
  })
);
