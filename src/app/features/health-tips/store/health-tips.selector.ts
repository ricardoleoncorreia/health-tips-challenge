import { createSelector } from '@ngrx/store';

import { AppStore } from '@core/store/app-store';

export const selectHealthTips = (state: AppStore) => state.healthTips;
export const selectHealthTipsList = createSelector(selectHealthTips, healthTips => healthTips.list);
export const selectHealthTipsFailed = createSelector(selectHealthTips, healthTips => healthTips.failed);
export const selectHealthTipsLoading = createSelector(selectHealthTips, healthTips => healthTips.loading);

export const selectHealthTipById = (props: { id: number; }) => createSelector(selectHealthTipsList, list => list.find(item => item.id === props.id));
