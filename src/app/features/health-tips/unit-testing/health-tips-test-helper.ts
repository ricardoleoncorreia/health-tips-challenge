import { HealthTipType } from '@features/health-tips/enums/health-tip-types';
import { HealthTipDto } from '@features/health-tips/interfaces/health-tips-dto';
import { HealthTip } from '@features/health-tips/models/health-tip';

export class HealthTipsTestHelper {
  readonly tipsDto: HealthTipDto;
  readonly tipsDtoList: HealthTipDto[];
  readonly tips: HealthTip;
  readonly tipsList: HealthTip[];

  constructor() {
    const randomPickedTypes = [HealthTipType.DoctorHealthTip, HealthTipType.FamilyHealthTip];

    this.tipsDtoList = randomPickedTypes.map((type, id) => this.generateDummyHealthTipDto(id + 1, type));
    this.tipsList = this.tipsDtoList.map(tipsDto => new HealthTip(tipsDto));
    this.tipsDto = this.tipsDtoList[0];
    this.tips = this.tipsList[0];
  }

  private generateDummyHealthTipDto(id: number, type: HealthTipType): HealthTipDto {
    return {
      datetime: 'datetime',
      downVotes: 0,
      id,
      level: 'level',
      text: 'text',
      title: 'title',
      type,
      upVotes: 0
    };
  }
}
