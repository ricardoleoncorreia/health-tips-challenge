import { NgModule } from '@angular/core';

import { ErrorMessageComponent } from './components/error-message/error-message.component';

@NgModule({
  declarations: [ErrorMessageComponent],
  exports: [ErrorMessageComponent]
})
export class SharedModule { }
